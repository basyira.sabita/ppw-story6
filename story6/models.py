from django.db import models

# Create your models here.   
class Kegiatan(models.Model):
    kegiatan = models.CharField('Kegiatan', max_length=100)

    def __str__(self):
        return self.kegiatan

class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama = models.CharField('Peserta', max_length=50)
