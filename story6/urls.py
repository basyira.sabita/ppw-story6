from django.urls import path
from story6.views import formKegiatan, listKegiatan

urlpatterns = [
    path('form-kegiatan/', formKegiatan),
    path('', listKegiatan),
]