from django.test import TestCase, Client
from .models import Kegiatan
from .views import listKegiatan
from django.http import HttpRequest

# Create your tests here.
class TestStory6(TestCase):

    def test_url_form_kegiatan(self):
        response = Client().get('/form-kegiatan/')
        self.assertEquals(200, response.status_code)

    def test_template_form_kegiatan(self):
        response = Client().get('/form-kegiatan/')
        self.assertTemplateUsed(response, 'form_kegiatan.html')
    
    def test_views_form_kegiatan(self):
        response = Client().get('/form-kegiatan/')
        isi_html = response.content.decode('utf8') #di decode karena response.content balikin bytestring

        self.assertIn("ISI KEGIATAN", isi_html)
        self.assertIn('<input type="submit" value="Submit" class="btn btn-primary click">', isi_html) 
        #self.assertIn('<button id="kirim" type="submit" name="kirim">Submit</button>', isi_html)
        #self.assertIn('<form action="/" method="POST">', isi_html)
        self.assertIn('<a href="/"><button>Lihat Kegiatan</button></a>', isi_html)
    
    def test_url_list_kegiatan(self):
        response = Client().get("")
        self.assertEquals(200, response.status_code)

    def test_template_list_kegiatan(self):
         response = Client().get("")
         self.assertTemplateUsed(response, "list_kegiatan.html")
    
    def test_views_list_kegiatan(self):
        response = Client().get("")
        isi_html = response.content.decode('utf8') #di decode karena response.content balikin bytestring
        self.assertIn("LIST KEGIATAN", isi_html)
        self.assertIn('<a href="/form-kegiatan"><button>Isi Kegiatan</button></a>', isi_html)

    def test_model_form_kegiatan(self):
        Kegiatan.objects.create(kegiatan="Ngoding")
        jumlah = Kegiatan.objects.all().count()
        self.assertEquals(jumlah, 1)

    def test_create_kegiatan_form(self):
        Kegiatan.objects.create(kegiatan="astaghfirullah")
        request = HttpRequest()
        response = listKegiatan(request)
        isi_html = response.content.decode('utf8')
        self.assertIn("astaghfirullah", isi_html)

    def test_kirim_post_peserta(self):
        Kegiatan.objects.create(kegiatan="astaghfirullah")
        arg = {
            'nama' : 'bita', 'kegiatan':'astaghfirullah'
        }
        response = Client().post("", arg)
        isi_html = response.content.decode('utf8')
        self.assertIn("bita", isi_html)
        self.assertIn("astaghfirullah", isi_html)

    # def test_kirim_post_kegiatan(self):
    #     arg = {
    #         'kegiatan' : 'nangis'
    #     }
    #     response = Client().post("/form-kegiatan", data = arg)
    #     self.assertEqual(response.status_code, 301)

