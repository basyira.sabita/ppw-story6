from django.forms import ModelForm
from .models import Kegiatan, Peserta

class FormKegiatan(ModelForm):
    def __init__ (self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        for _, value in self.fields.items():
            value.widget.attrs['placeholder'] = value.help_text
    
    class Meta:
        model = Kegiatan
        fields = ['kegiatan']

class FormPeserta(ModelForm):
    def __init__ (self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        for _, value in self.fields.items():
            value.widget.attrs['placeholder'] = value.help_text
    
    class Meta:
        model = Peserta
        fields = '__all__'