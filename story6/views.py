from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Kegiatan, Peserta
from .forms import FormPeserta, FormKegiatan

# Create your views here.
def formKegiatan(request):
    form = FormKegiatan(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/')

    else :
        return render(request, 'form_kegiatan.html', {'form':form})


def listKegiatan(request):
    dataKegiatan = Kegiatan.objects.all()
    dataPeserta = Peserta.objects.all()
    form = FormPeserta(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return render(request, 'list_kegiatan.html', {'dataKegiatan':dataKegiatan, 'dataPeserta':dataPeserta, 'form':form})
    else:
        return render(request, 'list_kegiatan.html', {'dataKegiatan':dataKegiatan, 'dataPeserta':dataPeserta, 'form':form})